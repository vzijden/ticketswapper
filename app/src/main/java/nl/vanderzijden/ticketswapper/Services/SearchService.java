package nl.vanderzijden.ticketswapper.Services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.webkit.CookieManager;
import nl.vanderzijden.ticketswapper.Activities.MainActivity;
import nl.vanderzijden.ticketswapper.R;
import nl.vanderzijden.ticketswapper.model.Event;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by vincent on 19-10-16.
 * <p>
 * Background service for searching tickets on Ticketswap.
 * I would have preferred to use android's alarmmanager instead of a thread to call the background service, however from android
 * 5 and up it isn't possible to call recurring alarms with a timeout less than 60 seconds.
 */

public class SearchService extends Service {
    public final static String KEY_EVENT = "url";

    static final long CRAWL_TIMEOUT = 20000;

    Event mEvent;

    public static Intent createIntent(Context context, Event event) {
        Intent intent = new Intent(context, SearchService.class);
        intent.putExtra(KEY_EVENT, event);
        return intent;
    }

    //    Notification to let the user know the service is still running
    void createOngoingNotification() {
        Intent stopIntent = new Intent(this, MainActivity.class);
        stopIntent.setAction(MainActivity.ACTION_STOP_SEARCH);

        NotificationCompat.Action action = new NotificationCompat.Action.Builder(
                R.drawable.ic_clear_black_24dp,
                getString(R.string.stop_searching),
                PendingIntent.getActivity(this, 0, stopIntent, 0)).build();

        Notification notification = new NotificationCompat.Builder(this)
                .setOngoing(true)
                .setContentTitle(getString(R.string.searching_tickets))
                .setContentText(getString(R.string.searching_for_tickets_for) + mEvent.getName())
                .setSmallIcon(R.drawable.ic_add_black_24dp)
                .addAction(action)
                .build();

        NotificationManagerCompat.from(this).notify(1, notification);
    }

//    Notify the MainActivity a ticket has been found
    void onFoundTicket() {
        Intent stopIntent = new Intent(this, MainActivity.class);
        stopIntent.setAction(MainActivity.ACTION_FOUND_TICKET);
        startActivity(stopIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationManagerCompat.from(this).cancelAll();
        searchTread.stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        mEvent = (Event) intent.getSerializableExtra(KEY_EVENT);
        createOngoingNotification();

        searchTread.start();

    }

    Thread searchTread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                checkForListings();
                try {
//                    To bad it needs to be like this since we can't use AlarmManager anymore
                    Thread.sleep(CRAWL_TIMEOUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    void checkForListings() {
        try {
//            Check if there are any new tickets offered on the mEvent page
            Document doc = Jsoup.connect(mEvent.getUrl()).get();
            Elements elements = doc.getElementsByClass("listings-item");

            for (Element element : elements) {
//                Distinguish between offered and sold tickets
                if (!element.hasClass("listings-item--not-for-sale")) {
                    Element offerLink = element.getElementsByTag("a").first();
                    String offerUrl = offerLink.attr("href");
                    if (tryReserve(offerUrl)) {
                        onFoundTicket();
                        return;
                    }
                }
            }

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            Intent viewTicketSwap = new Intent(Intent.ACTION_VIEW);
            viewTicketSwap.setData(Uri.parse(mEvent.getUrl()));
            startActivity(viewTicketSwap);
        }
    }

    boolean tryReserve(String ticketUrl) throws IOException {
//        Get the ticket's page and extract the reserve url from the form
        Element buyDoc = Jsoup.connect(getString(R.string.ticketswap) + ticketUrl)
                .header("Cookie", getSessionCookie())
                .get();

        Element form = buyDoc.getElementById("listing-reserve-form");
        String reserveUrl = form.attr("data-endpoint");
        final String token = form.getElementsByTag("input").first().attr("value");

        if (buyDoc.getElementById("listing-reserve-form") != null) {
            Connection.Response res = Jsoup.connect(getString(R.string.ticketswap) + reserveUrl)
                    .data("token", token)
                    .header("Cookie", getSessionCookie())
                    .data("amount", "1")
                    .ignoreContentType(true)
                    .method(Connection.Method.POST)
                    .timeout(5000)
                    .execute();

            Log.d("search",res.body());
            if (res.body().contains("succes"))
                return true;
        }

        return false;


    }

    //    Extract the session cookie from the webview
    String getSessionCookie() {
        String cookies = CookieManager.getInstance().getCookie(getString(R.string.ticketswap)).replaceAll("\\s+", "");

        String[] cookieArray = cookies.split(";");
        for (String cookie : cookieArray) {
            int seperatorIndex = cookie.indexOf("=");
            String cookieValue = cookie.substring(0, seperatorIndex);
            if (cookieValue.equals("session"))
                return cookie;
        }

        return "";
    }

}
