package nl.vanderzijden.ticketswapper.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by vincent on 18-10-16.
 *
 */

public class Event extends SugarRecord implements Serializable{
    String name;
    String url;

    public String getDate() {
        return date;
    }

    String date = "";
    int id;
    String wanted;
    String sold;

    public Event(String name, String url, String date, String wanted, String sold) {
        this.name = name;
        this.url = url;
        this.date = date;
        this.wanted = wanted;
        this.sold = sold;
    }

    public Event(String url) {
        this.url = url;
    }

    public Event() {
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWanted() {
        return wanted;
    }

    public String getSold() {
        return sold;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setId(int id) {
        this.id = id;
    }
}
