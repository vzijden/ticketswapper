package nl.vanderzijden.ticketswapper.model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import com.google.common.collect.Lists;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincent on 18-10-16.
 *
 * Observable model class for Events. Loads and saves the the events using the Sugar ORM framework.
 *
 */

public class Events extends BaseObservable implements Serializable {
    ObservableArrayList<Event> events;

    public Events(ObservableArrayList<Event> events) {
        this.events = events;
    }

    @Bindable
    public ObservableArrayList<Event> getEvents() {
        return events;
    }

    public void addEvent(Event event) {
        events.add(event);
        notifyChange();
        event.save();
    }

    public static Events loadEvents() {
        ObservableArrayList<Event> observableEvents = new ObservableArrayList<>();
        observableEvents.addAll(Lists.reverse(Event.listAll(Event.class)));
        return new Events(observableEvents);
    }
}
