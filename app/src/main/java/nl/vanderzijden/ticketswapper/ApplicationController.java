package nl.vanderzijden.ticketswapper;

import android.app.Application;
import com.orm.SugarContext;

/**
 * Created by Vincent van der Zijden on 22-10-16.
 *
 * Override of the Application to implement the Sugar framework
 */

public class ApplicationController extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }
}
