package nl.vanderzijden.ticketswapper.Activities;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import nl.vanderzijden.ticketswapper.R;
import nl.vanderzijden.ticketswapper.Services.SearchService;
import nl.vanderzijden.ticketswapper.databinding.ActivityMainBinding;
import nl.vanderzijden.ticketswapper.databinding.ItemEventBinding;
import nl.vanderzijden.ticketswapper.model.Event;
import nl.vanderzijden.ticketswapper.model.Events;

/**
 * Created by Vincent van der Zijden on 22-10-16.
 * <p>
 * MainActivity and view. Uses databinding for UI communication. Calls the SearchService and AddEventAcivity.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int RESULT_ADD_EVENT = 1;
    public static final String ACTION_STOP_SEARCH = "stop";
    public static final String ACTION_FOUND_TICKET = "found";

    Events mEvents;
    EventsAdapter mEventsAdapter = new EventsAdapter();
    ActivityMainBinding mbinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEvents = Events.loadEvents();

        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mEventsAdapter);

        mbinding.addEvent.setOnClickListener(this);

//        Intent called from the search service when it stops searching.
        if (getIntent().getAction().equals(ACTION_STOP_SEARCH))
            stopSearch();

        if (getIntent().getAction().equals(ACTION_FOUND_TICKET))
            onFoundTicket();

    }

    public void addEvent() {
        Intent addEvent = new Intent(this, AddEventActivity.class);
        startActivityForResult(addEvent, RESULT_ADD_EVENT);
    }

    void startSearch(Event event) {
        Intent serviceIntent = SearchService.createIntent(this, event);
        startService(serviceIntent);

        mbinding.setActiveEvent(event);
    }

    //    Notify the user a ticket has been found, opens Ticketswap.nl/cart on click.
    void onFoundTicket() {
        Intent viewChartIntent = new Intent(Intent.ACTION_VIEW);
        viewChartIntent.setData(Uri.parse(getString(R.string.ticketswap) + getString(R.string.cart)));

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.found_ticket))
                .setContentText(getString(R.string.touch_to_go_to_cart))
                .setSmallIcon(R.drawable.ic_add_black_24dp)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(PendingIntent.getActivity(this, 0, viewChartIntent, 0))
                .build();

        NotificationManagerCompat.from(this).notify(2, notification);

        stopSearch();
    }

    void stopSearch() {
        stopService(new Intent(this,SearchService.class));
        mbinding.setActiveEvent(null);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_ADD_EVENT && resultCode == RESULT_OK) {
            Event event = (Event) data.getSerializableExtra(AddEventActivity.KEY_EVENT);
            mEvents.addEvent(event);
            startSearch(event);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_event)
            addEvent();
    }

    class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

        class EventViewHolder extends RecyclerView.ViewHolder {
            ItemEventBinding binding;

            public EventViewHolder(ItemEventBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }

        @Override
        public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ItemEventBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.item_event, parent, false);
            return new EventViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(EventViewHolder holder, final int position) {
            holder.binding.setEvent(mEvents.getEvents().get(position));
            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startSearch(mEvents.getEvents().get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return mEvents.getEvents().size();
        }
    }

}