package nl.vanderzijden.ticketswapper.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.*;
import android.widget.FrameLayout;
import android.widget.Toast;
import nl.vanderzijden.ticketswapper.R;
import nl.vanderzijden.ticketswapper.databinding.ActivityAddEventBinding;
import nl.vanderzijden.ticketswapper.model.Event;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * Created by vincent on 18-10-16.
 * <p>
 * Activity to extract an mEvent from ticketswap.nl using android´s webview.
 * Because of an incompatibility with androids webview and the facebook login a custom implementation of android's WebView and
 * WebChromeView is required
 */

public class AddEventActivity extends AppCompatActivity {
    public static String KEY_EVENT = "mEvent";

    WebView mWebView, mPopWebview;
    ViewGroup mViewParent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityAddEventBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_add_event);
        mWebView = binding.webview;
        mViewParent = binding.container;
        createWebView();
    }

    void createWebView() {
        CookieManager.getInstance().setAcceptCookie(true);
        mWebView.setWebViewClient(uriWebViewClient);
        mWebView.setWebChromeClient(uriChromeClient);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.loadUrl(getString(R.string.ticketswap));
    }

    /**
     *  Extracts the event from the event page
     */

    void getEvent(final String url) {
        new AsyncTask<Void, Void, Event>() {
            @Override
            protected Event doInBackground(Void... params) {
                try {
                    Element doc = Jsoup.connect(url).get();
                    return new Event(
                            doc.getElementsByTag("h1").text(),
                            url,
                            doc.getElementsByClass("date").first().text(),
                            doc.getElementsByClass("counter-wanted").first().getElementsByClass("counter-value").text(),
                            doc.getElementsByClass("counter-sold").first().getElementsByClass("counter-value").text());

                } catch (IOException | NullPointerException e) {
                    e.printStackTrace();
                    Toast.makeText(AddEventActivity.this, R.string.error_getting_event, Toast.LENGTH_SHORT).show();
                }

//                Fallback for when the exraction fails, creates an event contain only the url.
                return new Event(url);
            }

            @Override
            protected void onPostExecute(Event event) {
                super.onPostExecute(event);

                Intent result = new Intent();
                result.putExtra(KEY_EVENT, event);
                setResult(RESULT_OK, result);
                finish();
            }
        }.execute();
    }

    /**
     * Custom WebView to handle the facebook login popup and extract the mEvent page url
     * when an mEvent has been selected.
     */

    WebViewClient uriWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();

//            Close popup when redirected to ticketswap
            if (host.contains(getString(R.string.ticketswap))) {
                if (mPopWebview != null) {
                    mPopWebview.setVisibility(View.GONE);
                    mViewParent.removeView(mPopWebview);
                    mPopWebview = null;
                }
                return false;
            }

            if (host.equals("m.facebook.com")) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//            Ticketswap.nl/event/ is used for event pages. Extract the event when an event page url has been selected.
            if (url.contains("/event/"))
                getEvent(url);
            return super.shouldInterceptRequest(view, url);
        }
    };

    /**
     * Custom WebChromeClient to handle the facebook popup
     */

    WebChromeClient uriChromeClient = new WebChromeClient() {
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            mPopWebview = new WebView(AddEventActivity.this);
            mPopWebview.setVerticalScrollBarEnabled(false);
            mPopWebview.setHorizontalScrollBarEnabled(false);
            mPopWebview.setWebViewClient(uriWebViewClient);
            mPopWebview.getSettings().setJavaScriptEnabled(true);
            mPopWebview.getSettings().setSavePassword(false);
            mPopWebview.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            mViewParent.addView(mPopWebview);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mPopWebview);
            resultMsg.sendToTarget();

            return true;
        }
    };

}